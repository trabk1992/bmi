//
//  BMI.swift
//  BMI
//
//  Created by Nguyen Tuan Anh Tra on 3/5/21.
//

import Foundation

class Bmi {
    
    private var bmiIndex = 0.0 ;
    private var status = "";
    
    func getBmiIndex() -> Double {
        return bmiIndex;
    }
    
    func getSatus() -> String {
        if bmiIndex < 18.5 {
            status = "underweight";
        } else if bmiIndex < 25{
            status = "normal weight";
        } else if bmiIndex < 30 {
            status = "overweight";
        } else {
            status = "obese";
        }
        return status;
    }
    
    func calculateBmi(height:Double, weight:Double) {
        bmiIndex = weight/(height*height);
    }
}
