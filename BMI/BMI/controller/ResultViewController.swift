//
//  ResultViewController.swift
//  BMI
//
//  Created by Nguyen Tuan Anh Tra on 3/4/21.
//

import UIKit

class ResultViewConller : UIViewController {
    
    
    @IBOutlet weak var bmiIndexLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var recalculateBtn: UIButton!
    
    var bmiIndex = 0.0;
    var status = "";
    override func viewDidLoad() {
        bmiIndexLabel.text = String(format:"%.2f", bmiIndex);
        statusLabel.text = status;
    }
    
    
    @IBAction func recalculate(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
}
