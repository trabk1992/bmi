//
//  ViewController.swift
//  BMI
//
//  Created by Nguyen Tuan Anh Tra on 3/4/21.
//

import UIKit

class MainViewController: UIViewController {
    

    @IBOutlet weak var weightSlider: UISlider!
    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var bmiBtn: UIButton!

    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    var weight = 0.0;
    var height = 0.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bmiBtn.backgroundColor = .green;
    }

    @IBAction func caculateBmi(_ sender: UIButton) {
        performSegue(withIdentifier: "goToResult", sender: self);
    }
    
    
    @IBAction func onWeightChanged(_ sender: UISlider) {
        weight = Double(sender.value);
        weightLabel.text = String(format:"%.2f", weight) + " Kg";
    }
    
    
    @IBAction func onHeightChanged(_ sender: UISlider) {
        height = Double(sender.value);
        heightLabel.text = String(format:"%.2f", height) + " Cm";
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let bmi = Bmi();
            let height = self.height / 100;
            bmi.calculateBmi(height: height, weight: self.weight);
            let vc = segue.destination as! ResultViewConller;
            vc.bmiIndex = bmi.getBmiIndex();
            vc.status = bmi.getSatus();
        }
    }

}

